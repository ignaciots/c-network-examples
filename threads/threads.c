/*
 * threads.c
 *
 * Copyright 2017 nacho <nacho@nacho-mint>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define NUMBER_OF_OPERATIONS 1000

void * thread_function(void *);

int main(int argc, char * argv[])
{
    pthread_t thread1_id;
    pthread_t thread2_id;

    long thread1_number = 1;
    long thread2_number = -1;
    long counter        = NUMBER_OF_OPERATIONS;

    void * thread1_args = calloc(2, sizeof(void *));
    void * thread2_args = calloc(2, sizeof(void *));

    ((long **) thread1_args)[0] = &counter;
    memcpy(thread2_args, thread1_args, sizeof(void *) * 2);
    ((long **) thread1_args)[1] = &thread1_number;
    ((long **) thread2_args)[1] = &thread2_number;

    pthread_create(&thread1_id, NULL, thread_function, thread1_args);
    pthread_create(&thread2_id, NULL, thread_function, thread2_args);

    pthread_join(thread1_id, NULL);
    pthread_join(thread2_id, NULL);

    printf("Counter final value: %ld\n", counter);

    return EXIT_SUCCESS;
}

void * thread_function(void * argv)
{
    long * counter_ptr        = ((long **) argv)[0];
    long * thread_number      = ((long **) argv)[1];
    unsigned long thread_id   = (unsigned long) pthread_self();
    long number_of_operations = 0;

    while (number_of_operations < NUMBER_OF_OPERATIONS) {
        long updated_counter_value = *counter_ptr + *thread_number;
        *counter_ptr = updated_counter_value;
        printf("Thread [%lu]:\nCounter: %ld\nThread operation number: %ld\n",
          thread_id, *counter_ptr, *thread_number);
        number_of_operations++;
    }

    return NULL;
}
