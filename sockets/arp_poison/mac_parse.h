/*
 * MAC parse constants and function declarations
 */

#define MAC_VALID_NUMBERS "0123456789ABCDEF"
#define MAC_SEPARATOR     ':'
#define MAC_STR_SIZE      17

/**
 * Parses a MAC address in the format XX:XX:XX:XX:XX:XX and stores it in mac_dst.
 * mac_dst: The buffer where the converted address will be stored.
 * mac_str: The input MAC address.
 * Return: Non-zero if the parse is successful, 0 if the parse fails.
 */
int parse_mac(unsigned char * mac_dst, char * mac_str);

/**
 * Checks if a character is a valid MAC number.
 * character: The character to be checked.
 * Return: Non-zero if the character is a MAC number, 0 if it is not.
 */
int is_valid_number(char character);

/**
 * Checks if a character is a MAC separator.
 * character: The character to be checked.
 * Return: Non-zero if the character is a MAC separator, 0 if it is not.
 */
int is_mac_separator(char character);

/**
 * Checks if a string is a valid MAC address.
 * mac_str: The input string.
 * Return: Non zero if is a valid MAC address, 0 otherwise.
 */
int is_valid_mac(char * mac_str)
