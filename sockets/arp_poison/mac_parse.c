/*
 * MAC input parser
 */

#include <string.h>
#include <stdlib.h>
#include "mac_parse.h"

int parse_mac(unsigned char * mac_dst, char * mac_str)
{
    int valid_mac = is_valid_mac(mac_str);

    if (valid_mac) {
        convert_mac(mac_dst, mac_str);
        return 1;
    } else {
        return 0;
    }
}

int is_valid_number(char character)
{
    int i;
    int is_valid = 0;

    for (i = 0; i < strlen(MAC_VALID_NUMBERS) && !is_valid; i++) {
        if (MAC_VALID_NUMBERS[i] == character) {
            is_valid = 1;
        }
    }
    return is_valid;
}

int is_mac_separator(char character)
{
    return character == MAC_SEPARATOR;
}

int is_valid_mac(char * mac_str)
{
    if (strlen(mac_str) != MAC_STR_SIZE) return 0;

    int i;
    for (i = 0; i < MAC_STR_SIZE; i++) {
        char current_char = mac_str[i];
        if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14) { // Index location of the mac separators
            if (!is_mac_separator(current_char)) {
                return 0;
            }
        } else {
            if (!is_valid_number(current_char)) {
                return 0;
            }
        }
    }

    return 1;
}

void convert_mac(unsigned char * mac_dst, char * mac_str)
{ }
