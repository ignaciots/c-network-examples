/*
 * ARP Poison function declarations and constants
 */

#define ASK_INTERFACE_INPUT       "Please input the interface name: "
#define ASK_INTERFACE_INPUT_ERROR "An error has occured during the input.\nPlease input the interface name again: "

#define IP_DOT_MAX_SIZE           15
#define ASK_IP_INPUT_TARGET       "Please input the target address: "
#define ASK_IP_INPUT_AP           "Please input the target access point address: "
#define ASK_IP_INPUT_ERROR        "Invalid IP address.\nPlease input a valid IP address: "

#define ERROR_SOCKET_OPEN         "Error opening socket"
#define ERROR_IOCTL_INDEX         "Error ioctl() on ifreq index"
#define ERROR_IOCTL_ADDRESS       "Error ioctl() on ifreq address"
#define ERROR_SOCKET_SEND         "Error sendto() on socket"

/**
 * Gets the interface name from stdin.
 * The function will only read IFNAMSIZ characters at most.
 * Return: a pointer containing the allocated interface name, NULL if an error occurs.
 */
char * get_interface_name();

/**
 * Asks the user for the network interface name until a correct value is provided.
 * input_message: The input message.
 * error_message: The input error message.
 * Return: the valid interface name.
 */
char * ask_interface_name(const char * input_message, const char * error_message);

/**
 * Gets the IP in dot notation from stdin and stores it in inp.
 * Return: zero on error, non-zero otherwise.
 */
int get_ip_address(struct in_addr * inp);

/**
 * Asks the user for the IP target until a correct value is provided.
 * Return: the IP as an int in network byte order.
 */
int ask_ip_address(const char * input_message, const char * error_message);

/**
 * Reads a line from stdin up to buffer_size -1 and stores it in line_buffer.
 * The string will be null terminated
 * line_buffer: The string buffer.
 * buffer_size: the size of line_buffer.
 * Return: line_buffer or NULL if an error occurs.
 */
char * read_line(char * line_buffer, int buffer_size);

/**
 * Opens a raw socket.
 * Return: the socket file descriptor, -1 on error
 */
int open_raw_socket();

/**
 * Sets the interface index on interface.
 * interface: the ifreq structure where the index will be set.
 * interface_name: the interface name.
 * socket_fd: the socket file descriptor.
 * Return: -1 on error, non-error otherwise.
 */
int set_interface_index(struct ifreq * interface, const char * interface_name, int socket_fd);

/**
 * Sets the interface hardware address on interface.
 * interface: the ifreq structure where the address will be set.
 * interface_name: the interface name.
 * socket_fd: the socket file descriptor.
 * Return: -1 on error, non-error otherwise.
 */
int set_interface_address(struct ifreq * interface, const char * interface_name, int socket_fd);

/**
 * Creates an ethernet packet with the given parameters.
 * packet_buffer: The buffer where the packet will be stored. Its size must be enough to accept an ethernet packet.
 * packet_offset: The buffer offset where the packet will be stored.
 * shost: The source host address with 6 bytes of size.
 * dhost: The destination host address with 6 bytes of size.
 * Return: The pointer to the ethernet packet.
 */
struct ether_header * create_ethernet_packet(unsigned char * packet_buffer, int packet_offset, unsigned char * shost,
  unsigned char * dhost);

/**
 * Creates an ARP packet with the given parameters.
 * packet_buffer: The buffer where the packet will be stored. Its size must be enough to accept an ethernet packet.
 * packet_offset: The buffer offset where the packet will be stored.
 * sha: The sender hardware address with 6 bytes of size.
 * spa: The sender protocol address with 4 bytes of size.
 * tha: The target hardware address with 6 bytes of size.
 * tpa: The target protocol address with 4 bytes of size.
 * Return: The pointer to the ARP packet.
 */
struct arp_header * create_arp_packet(unsigned char * packet_buffer, int packet_offset, unsigned char * sha,
  unsigned char * spa, unsigned char * tha, unsigned char * tpa);

/**
 * Creates a link layer socket address.
 * ifindex: The interface index.
 * hardware_address: The interface hardware address.
 * Return: the newly created link layyer socket address.
 */
struct sockaddr_ll * create_sockaddr_ll(int ifindex, unsigned char * hardware_address);

/**
 * Sends a packet through an interface.
 * socket_fd: The socket used to send the packet.
 * packet_buffer: The sent packet.
 * packet_len: The length of the packet.
 * socket_ll: A structure with the index and the hardware address of the interface.
 * Return: On success, number of bytes sent. On error, -1.
 */
int send_packet(int socket_fd, unsigned char * packet_buffer, int packet_len, struct sockaddr_ll * socket_ll);
