/*
 * ARP packet structure and constants
 */

#define ARP_HTYPE_ETHERNET 0x01   // Ethernet hardware type
#define ARP_PTYPE_IPV4     0x0800 // IPv4 protocol type
#define ARP_HLEN_ETHERNET  0x06   // Ethernet hardware address length
#define ARP_PLEN_IPV4      0x04   // IPv4 protocol address length
#define ARP_OPER_REQUEST   0x01   // Request operation code
#define ARP_OPER_REPLY     0x02   // Reply operation code

/* ARP packet structure */
struct arp_header {
    unsigned short htype;                  // Hardware type (e.g. Ethernet)
    unsigned short ptype;                  // Protocol type
    unsigned char  hlen;                   // Hardware address length in octects (e.g. 6 for ethernet)
    unsigned char  plen;                   // Protocol address length in octets (e.g. 4 for IPv4)
    unsigned short oper;                   // Operation code
    unsigned char  sha[ARP_HLEN_ETHERNET]; // Sender hardware address
    unsigned char  spa[ARP_PLEN_IPV4];     // Sender protocol address
    unsigned char  tha[ARP_HLEN_ETHERNET]; // Target hardware address
    unsigned char  tpa[ARP_PLEN_IPV4];     // Target protocol address
};
