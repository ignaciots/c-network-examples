#include <netinet/tcp.h>
#include <netinet/ip.h>
#include "syn_flood.h"

int main(int argc, char * argv) {
  unsigned char packet_buffer[PACKET_BUFFER_SIZE];
  struct iphdr * ip_header = (struct iphdr *) packet_buffer;
  struct tcphdr * tcp_header = (struct tcphdr *) (packet_buffer + sizeof(struct iphdr));

  int socket_fd = socket(AF_INET, SOCK_RAW, IPPROTO_TCP);
  ip_header->ihl = 5;
  ip_header->version = 4;
  ip_header->tos = 0;
  ip_header->tot_len = sizeof(struct iphdr) + sizeof(struct tcphdr);
  ip_header->id=htons(54321);
  return 0;
}
