#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/ip.h>
#include <linux/icmp.h>
#include <linux/if_packet.h>
#include <netinet/ether.h>
#include <arpa/inet.h>
#include "iphdr_checksum.h"

#define INTERFACE_NAME "wlp4s0"
#define ICMP_DATA_LEN  100

void print_socket(const struct sockaddr_ll *);
void print_char_arr(const char *, int, char *);

int main(int argc, char * argv[])
{
    char dest_mac_address[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
    int socket_fd;
    struct ifreq interface_index;
    struct ifreq interface_mac;
    int tx_len = sizeof(struct ether_header) + sizeof(struct iphdr) + sizeof(struct icmphdr) + ICMP_DATA_LEN;
    struct ether_header * ethernet;
    struct iphdr * ip;
    struct icmphdr * icmp;
    char * icmp_data;
    char * packet_buffer;
    struct sockaddr_ll socket_address;

    // Open raw socket
    if ((socket_fd = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
        perror("Socket FD Error");
        exit(EXIT_FAILURE);
    }

    // Get interface_index index
    memset(&interface_index, 0, sizeof(struct ifreq));
    strncpy(interface_index.ifr_name, INTERFACE_NAME, IFNAMSIZ - 1);
    if (ioctl(socket_fd, SIOCGIFINDEX, &interface_index) == -1) {
        perror("ioctl IFINDEX Error");
        exit(EXIT_FAILURE);
    }
    // Get hardware address index
    memset(&interface_mac, 0, sizeof(struct ifreq));
    strncpy(interface_mac.ifr_name, INTERFACE_NAME, IFNAMSIZ - 1);
    if (ioctl(socket_fd, SIOCGIFHWADDR, &interface_mac) == -1) {
        perror("ioctl IFHWADDR Error");
        exit(EXIT_FAILURE);
    }

    packet_buffer = malloc(tx_len);
    ethernet      = (struct ether_header *) packet_buffer;
    ip        = (struct iphdr *) (packet_buffer + sizeof(struct ether_header));
    icmp      = (struct icmphdr *) (packet_buffer + sizeof(struct ether_header) + sizeof(struct iphdr));
    icmp_data = (char *) (packet_buffer + sizeof(struct ether_header) + sizeof(struct iphdr) + sizeof(struct icmphdr));
    strncpy(icmp_data, "Probando...", ICMP_DATA_LEN);

    memcpy(ethernet->ether_shost, interface_mac.ifr_hwaddr.sa_data, 6);
    memcpy(ethernet->ether_dhost, dest_mac_address, 6);

    ethernet->ether_type = htons(ETH_P_IP);

    ip->ihl      = 5;
    ip->version  = 4;
    ip->tot_len  = htons(sizeof(struct iphdr) + sizeof(struct icmphdr) + ICMP_DATA_LEN);
    ip->protocol = 0x01;
    ip->saddr    = inet_addr("192.168.1.109");
    ip->daddr    = inet_addr("192.168.1.1");
    ip->ttl      = 100;
    ip->check    = iphdr_checksum(ip, sizeof(struct iphdr));

    icmp->type     = ICMP_ECHO;
    icmp->checksum = htons(0x161f);

    /* Index of the network device */
    socket_address.sll_ifindex = interface_index.ifr_ifindex;
    /* Address length*/
    socket_address.sll_halen = ETH_ALEN;
    /* Destination MAC */
    memcpy(socket_address.sll_addr, interface_mac.ifr_hwaddr.sa_data, 6);

    /* Send packet */
    if (sendto(socket_fd, packet_buffer, tx_len, 0, (struct sockaddr *) &socket_address,
      sizeof(struct sockaddr_ll)) < 0)
    {
        perror("Socket sendto Error");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
} /* main */

void print_socket(const struct sockaddr_ll * socket)
{
    printf("Family: %hu\n", socket->sll_family);
    printf("Protocol: %hu\n", socket->sll_protocol);
    printf("Interface index: %d\n", socket->sll_ifindex);
    printf("ARP HW Type: %hu\n", socket->sll_hatype);
    printf("Packet type: %d\n", socket->sll_pkttype);
    printf("Address length: %d\n", socket->sll_halen);
    unsigned int i;
    for (i = 0; i < sizeof(socket->sll_addr) / sizeof(unsigned char); i++) {
        printf("MAC Address octet %d: %d\n", i, socket->sll_addr[i]);
    }
}

void print_char_arr(const char * arr, int len, char * name)
{
    int i;

    for (i = 0; i < len; i++) {
        printf("%s[%d]: %d\n", name, i, arr[i]);
    }
}
