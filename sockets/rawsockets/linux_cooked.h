#define LINUX_COOKED_PTK_UNICAST   0x00
#define LINUX_COOKED_ADDR_ETHER    0x0304
#define LINUX_COOKED_ADDRLEN_ETHER 0x06
#define LINUX_COOKED_PROTO_IPV4    0x0800

struct linux_cooked {
    unsigned short pkt_type;
    unsigned short addr_type;
    unsigned short addr_len;
    unsigned char  src[LINUX_COOKED_ADDRLEN_ETHER];
    unsigned short proto;
};
