#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include "listen_raw_socket.h"

int main(int argc, char * argv[])
{
    int socket_fd = open_raw_socket();

    handle_socket_error(socket_fd, "Socket socket() error");
    return EXIT_SUCCESS;
}

int open_raw_socket()
{
    return socket(AF_PACKET, SOCK_RAW, 0);
}

void handle_socket_error(int error_number, const char * error_message)
{
    if (error_number == -1) {
        perror(error_message);
        exit(EXIT_FAILURE);
    }
}
