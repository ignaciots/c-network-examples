/*
 * iphdr_checksum.c
 *
 * Copyright 2017 Ignacio <ignacio@mint-Aspire-V3>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/ip.h>
#include <linux/if_packet.h>
#include <net/if.h>
#include <arpa/inet.h>
#include "iphdr_checksum.h"


unsigned short iphdr_checksum(const struct iphdr * ip, unsigned int len)
{
    char * data = (char *) ip;
    unsigned int accumulator = 0xFFFF;
    unsigned int i;

    for (i = 0; i + 1 < len; i += 2) {
        unsigned int word;
        memcpy(&word, data + i, 2);
        accumulator += ntohs(word);
        if (accumulator > 0xFFFF) {
            accumulator -= 0xFFFF;
        }
    }

    if (len & 1) {
        unsigned int word = 0;
        memcpy(&word, data + len - 1, 1);
        accumulator += ntohs(word);
        if (accumulator > 0xffff) {
            accumulator -= 0xffff;
        }
    }

    return htons(~accumulator);
}
